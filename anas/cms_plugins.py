from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import gettext_lazy as _
from . import models

# @plugin_pool.register_plugin
class AnasPlugin(CMSPluginBase):
    model = models.Poem
    render_template = "mypoems.html"
    cache=False  
    render_plugin =True

    def render(self, context,
               instance, placeholder):
        context.update({"instance": instance})

        return context


plugin_pool.register_plugin(AnasPlugin)